package com.example.jooq.repository;

import com.example.jooq.dto.AccountDto;
import com.example.jooq.dto.PersonDto;
import com.example.jooq.dto.PersonResultDto;
import com.example.jooq.mappers.AccountMapper;
import com.example.jooq.mappers.PersonMapper;
import lombok.AllArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.jooq.dto.generated.tables.Account.ACCOUNT;
import static com.example.jooq.dto.generated.tables.Person.PERSON;

@Repository
@AllArgsConstructor
public class PersonRepository {

    private final DSLContext dslContext;
    private final PersonMapper personMapper;
    private final AccountMapper accountMapper;


    public List<PersonResultDto> findAllPersons() {
        final Map<PersonDto, List<AccountDto>> personsResult = dslContext
                .selectFrom(PERSON.leftJoin(ACCOUNT).on(PERSON.ID.eq(ACCOUNT.ID_PERSON)))
                .fetchGroups(
                        record -> personMapper.map(record),
                        record -> accountMapper.map(record));
        return prepareResult(personsResult);
    }

    public PersonResultDto findPersonById(Integer id) {
        final Map<PersonDto, List<AccountDto>> personResult = dslContext
                .selectFrom(PERSON.leftJoin(ACCOUNT).on(PERSON.ID.eq(ACCOUNT.ID_PERSON)))
                .where(PERSON.ID.eq(id))
                .fetchGroups(
                        record -> personMapper.map(record),
                        record -> accountMapper.map(record));
        return personResult.entrySet().stream()
                .map(personDtoEntry -> PersonResultDto.builder()
                        .person(personDtoEntry.getKey())
                        .accounts(personDtoEntry.getValue())
                        .build())
                .findFirst()
                .orElse(null);
    }

    public List<PersonResultDto> findBatchOfPersons(int offset, int limit) {
        final Map<PersonDto, List<AccountDto>> personsResult = dslContext
                .selectFrom(PERSON.leftJoin(ACCOUNT).on(PERSON.ID.eq(ACCOUNT.ID_PERSON)))
                .offset(offset)
                .limit(limit)
                .fetchGroups(
                        record -> personMapper.map(record),
                        record -> accountMapper.map(record));
        return prepareResult(personsResult);
    }

    private List<PersonResultDto> prepareResult(Map<PersonDto, List<AccountDto>> personsResult) {
        return personsResult.entrySet().stream()
                .map(personDtoEntry -> PersonResultDto.builder()
                        .person(personDtoEntry.getKey())
                        .accounts(personDtoEntry.getValue())
                        .build())
                .collect(Collectors.toList());
    }

}
