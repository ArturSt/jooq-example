package com.example.jooq.dto;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class PersonResultDto {

    private PersonDto person;
    private List<AccountDto> accounts = new ArrayList<>();

}
