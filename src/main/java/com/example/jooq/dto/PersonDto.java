package com.example.jooq.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonDto {

    private Integer id;
    private String firstName;
    private String lastName;

}
