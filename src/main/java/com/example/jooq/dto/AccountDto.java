package com.example.jooq.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountDto {

    private Integer id;
    private Integer idPerson;
    private Integer cardNumber;
    private Integer value;

}
