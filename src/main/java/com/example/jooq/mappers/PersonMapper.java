package com.example.jooq.mappers;

import com.example.jooq.dto.PersonDto;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;

import static com.example.jooq.dto.generated.tables.Person.PERSON;

@Component
public class PersonMapper implements RecordMapper<Record, PersonDto> {

    @Override
    public PersonDto map(Record record) {
        return PersonDto.builder()
                .id(record.get(PERSON.ID))
                .firstName(record.get(PERSON.FIRST_NAME))
                .lastName(record.get(PERSON.LAST_NAME))
                .build();
    }

}
