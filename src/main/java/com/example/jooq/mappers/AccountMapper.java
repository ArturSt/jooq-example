package com.example.jooq.mappers;

import com.example.jooq.dto.AccountDto;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static com.example.jooq.dto.generated.tables.Account.ACCOUNT;

@Component
public class AccountMapper implements RecordMapper<Record, AccountDto> {

    @Override
    public AccountDto map(Record record) {
        final Integer accountId = record.get(ACCOUNT.ID);
        if (Objects.nonNull(accountId)) {
            return AccountDto.builder()
                    .id(accountId)
                    .idPerson(record.get(ACCOUNT.ID_PERSON))
                    .cardNumber(record.get(ACCOUNT.CARD_NUMBER))
                    .value(record.get(ACCOUNT.VALUE))
                    .build();
        } else {
            return null;
        }
    }

}
