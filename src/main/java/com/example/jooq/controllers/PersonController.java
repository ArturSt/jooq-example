package com.example.jooq.controllers;

import com.example.jooq.dto.PersonResultDto;
import com.example.jooq.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/mybatis/person")
public class PersonController {

    @Autowired
    private PersonService personService;


    @GetMapping()
    public ResponseEntity<List<PersonResultDto>> getAllPersons() {
        return ResponseEntity.ok(personService.getAllPersons());
    }

    @GetMapping(value = "/ids/{id}")
    public ResponseEntity<PersonResultDto> getPersonById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(personService.getPersonById(id));
    }

    @GetMapping(value = "/offset/{offset}/limit/{limit}")
    public ResponseEntity<List<PersonResultDto>> getBatchOfPersons(@PathVariable("offset") Integer offset,
                                                                   @PathVariable("limit") Integer limit) {
        return ResponseEntity.ok(personService.getBatchAllPersons(offset, limit));
    }

}
