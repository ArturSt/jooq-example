package com.example.jooq.services;

import com.example.jooq.dto.PersonResultDto;
import com.example.jooq.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@AllArgsConstructor
@Service
public class PersonService {

    private final PersonRepository personRepository;

    public List<PersonResultDto> getAllPersons() {
        return personRepository.findAllPersons();
    }

    public PersonResultDto getPersonById(Integer id) {
        return personRepository.findPersonById(id);
    }

    public List<PersonResultDto> getBatchAllPersons(int offset, int limit) {
        return personRepository.findBatchOfPersons(offset, limit);
    }

}
